----ROLL THE CREDITS----

sds1612 - The lead developer who programmed most of the app.

alfster2012 - A friend who helped create the website, edit and clean the UI of the app.

The fabulous people at "https://romannurik.github.io/AndroidAssetStudio/icons-launcher.html" for helping us make the logo.

!!! Many thanks to Google Design for their material design icons library. Finally an official place with material icons! :) !!!

----END CREDITS----
